#!/bin/bash

# Získání root práv
sudo -s

# Vytvoření databáze podle SQL souboru
mysql -u root --password='' < /vagrant/db.sql

exit 0