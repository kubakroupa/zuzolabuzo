# Virtuální síť

## Zadání

Vytvořte virtuální síť, která se skládá z DHCP serveru, DNS serveru a databáze.
K vytvoření virtuální sítě použijte nástroj Vagrant v kombinaci s nástrojem Ansible, který slouží k automatizaci zpracování jednotlivých skriptů.
V týmu si rozdělte role - co kdo bude dělat. K projektu pak sestavte dokumentaci.

## Tým

-   Štěpán Galle (Vedoucí, dokumentace, nějaké pokusy pomoct při dělání DNS, DHCP, MySQL)
-   Vladimír Kopejska (DNS, DHCP, dokumentace)
-   Martin Košek (MySQL, dokumentace)

## Použité technologie a programy

Vycházeli jsme ze zadání, takže jsme se rozhodli použít právě všechny v něm uvedené technologie.

### Vagrant

Stará se o správu jednotlivých virtuálních strojů, ale ne-emuluje je. Teoreticky se dá říct, že struktura celé sítě je vytvořena právě v něm.
Přes CLI se dá tedy velice jednoduše a efektivně spravovat celá virtuální síť.
Funguje na principu "boxů", které je uživatel shopný jednoduše stáhnout a poté aplikovat a instalovat na jednotlivá virtuální zařízení, která si definoval v konfiguračním souboru Vagrantu.
Těmto zařízením může mimo jiné přiřadit například statickou IP adresu, nebo "provision" soubor, který může býť buď přímo shell skript, nebo v našem případě Ansible "playbook".
Provision soubor se zpracuje při spouštění jednotlivých strojů, nebo také manuálním příkazem. Definicí úloh v tomto souborů je například možné jednoduše nainstalovat na jednotlivé stroje potřebné balíky, nebo konfiguraci služeb. Není pak tedy nutné pracovat na zařízeních jednotlivě a provádět všechny příkazy manuálně, ale je možné si připravit tuto automatizaci.

### Ansible

Nástroj sloužící k automatizaci jednotlivých úloh, které slouží k instalaci balíků a konfiguraci jednotlivých strojů. Je skvělou náhradou pro použití přímých shell skriptů (také kvůli zjednodušenému zápisu).

### CentOS

Linuxová distribuce, která je vhodná pro síťové účely. Na jednotlivých zařízeních jsme ho instalovali za použití Vagrant boxu (centos/7).

### VirtualBox

Program, který se stará o emulaci a běh jednotlivých virtuálních strojů.

## Výsledek práce

Zprovoznili jsme funkční DNS server, databázi a DHCP server, který ale bohužel nepřiřazuje IP adresu.

### DHCP

Vagrantfile konfigurace DHCP serveru.

```bash
config.vm.define "dhcp" do |dhcp|
	dhcp.vm.box = "centos/7"
	dhcp.vm.hostname = "dhcp"
	dhcp.vm.network "private_network", ip: "192.168.100.4"
	dhcp.vm.provision "ansible_local" do |ansible|
		ansible.playbook = "./provisions/dhcp.yml"
	 end
end
```

Ansible playbook obsahující úlohy pro nastavení DHCP serveru.

```yaml
---
- hosts: dhcp
  sudo: true
  vars:
  tasks:
      - name: Install kea (ISC DHCP) server
        become: yes
        become_user: root
        become_method: sudo
        yum:
            name: kea, mariadb, mariadb-server
            state: present

      - name: Start mysql deamon for socket connections
        become: yes
        become_user: root
        become_method: sudo
        service:
            name: mariadb
            state: started
            enabled: true

      - name: apply kea config
        become: yes
        become_user: root
        become_method: sudo
        copy:
            src: ../kea-dhcp/kea.conf
            dest: /etc/kea/kea.conf

      - name: Configure and start kea DHCP server IPV4
        become: yes
        become_user: root
        become_method: sudo
        service:
            name: kea-dhcp4
            state: started
            enabled: true

      - name: Configure and start kea DHCP server IPV6
        become: yes
        become_user: root
        become_method: sudo
        service:
            name: kea-dhcp6
            state: started
            enabled: true
```

### DNS
Vagrantfile konfigurace DNS serveru.

```bash
config.vm.define "dns" do |dns|
	dns.vm.box = "centos/7"
	dns.vm.hostname = "dns"
	dns.vm.network "private_network", ip: "192.168.100.3"
	 dns.vm.provision "ansible_local" do |ansible|
		ansible.playbook = "./provisions/dns.yml"
	 end
end
```

Ansible playbook obsahující úlohy pro nastavení DNS serveru.
```yaml
---
- hosts: dns
  sudo: true
  tasks:
      - name: install pdns
        yum:
            name: pdns, pdns-backend-mysql
            state: latest
      - name: apply pdns config
        copy:
            src: ../pdns/pdns.conf
            dest: /etc/pdns/pdns.conf
      - name: start the pdns service
        service:
            name: pdns
            state: started
            enabled: true
      - name: add zone
        command: pdnsutil create-zone kopejskagallekosek.net private.kopejskagallekosek.net
      - name: add record
        command: pdnsutil add-record kopejskagallekosek.net private A 192.168.100.2
      - name: add zone
        command: pdnsutil create-zone net kosekgallekopejska.net
      - name: add record
        command: pdnsutil add-record net kosekgallekopejska A 192.168.100.2
      - name: install recursor
        yum:
            name: pdns-recursor
            state: latest
      - name: apply recursor config
        copy:
            src: ../pdns/recursor.conf
            dest: /etc/pdns-recursor/recursor.conf
      - name: start recursor
        service:
            name: pdns-recursor
            state: started
            enabled: true

```

### Databáze

Vagrantfile konfigurace stroje s databází.

```bash
config.vm.define "sql" do |sql|
 	sql.vm.box = "centos/7"
 	sql.vm.hostname = "sql"
 	sql.vm.network "private_network", ip: "192.168.100.2"
	 sql.vm.provision "ansible_local" do |ansible|
		ansible.playbook = "./provisions/sql.yml"
	 end
	 sql.vm.provision "shell", path: "./provisions/sql.sh"
 end 
```

## Zdroje

-   Dokumentace Vagrantu
    -   https://www.vagrantup.com/docs/index.html
-   Dokumentace Ansible
    -   https://docs.ansible.com/
-   Dokumentace VirtualBox
    -   https://www.virtualbox.org/wiki/Documentation
